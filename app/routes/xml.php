<?php

function id_entidad_invalido($clave_entidad) {
  if (strlen($clave_entidad) != 2) {
    return true;
  }
}

//GET
$app->get('/entidades',function() use($app){
  $datos = array('entidades'=>array());
  $entidades=$app->db->query("select * from entidad");
  if(!$entidades){
    $app->halt(500, "Error: No se puede acceder a la informacion de entidades");
  }
  foreach($entidades as $entidades){
    $datos['entidades'][]=$entidades;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});



$app->get('/entidades/:clave_entidad', function($clave_entidad) use($app) {
  if (id_entidad_invalido($clave_entidad)) {
    $app->halt(400, "Error: la clave_entidad '$clave_entidad' no cumple con el formato correcto.");
  }

  $entidades = $app->db->query("select * from entidad where clave_entidad='$clave_entidad'");

  if (!$entidades) {
    $app->halt(500, "Error: No se ha podido localizar la entidad con clave '$clave_entidad'.");
  }
  foreach($entidades as $ent){
    $datos['entidades'][]=$ent;
  }
  $app->response->headers->set('Content-Type','application/xml');
  $app->response->setStatus(200);
  $app->render('xml.php',$datos);
});



$app->get('/entidades/:clave_entidad/municipios', function($clave_entidad) use($app) {
  $datos = array('municipios' => array());
 
  $municipios = $app->db->query("select * from municipio where clave_entidad='$clave_entidad'");
  
  if (!$municipios) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($municipios as $mun) {
    $datos['municipios'][] = $mun;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('municipios.php', $datos);
});

$app->get('/entidades/:clave_entidad/municipios/:clave_mun', function($clave_entidad,$clave_mun) use($app) {
  $datos = array('municipios' => array());
  $municipios = $app->db->query("select * from municipio where (clave_entidad='$clave_entidad' AND clave_mun='$clave_mun')");
  if (!$municipios) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($municipios as $mun) {
    $datos['municipios'][] = $mun;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('municipios.php', $datos);
});

$app->get('/entidades/:clave_entidad/municipios/:clave_mun/niveles', function($clave_entidad,$clave_mun) use($app) {
  $datos = array('niveles' => array());
  $niveles = $app->db->query("select distinct n.id_nivel , nivel from nivel as n inner join (select sub.id_nivel,tp.clave_mun from subnivel as sub inner join tasa_poblacional as tp on(sub.id_subnivel=tp.id_subnivel)) as temp on(temp.id_nivel= n.id_nivel) where temp.clave_mun='$clave_mun' order by n.id_nivel");
  if (!$niveles) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($niveles as $niv) {
    $datos['niveles'][] = $niv;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('niveles.php', $datos);
});

$app->get('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel', function($clave_entidad,$clave_mun,$id_nivel) use($app) {
  $datos = array('niveles' => array());
  $niveles = $app->db->query("select distinct n.id_nivel , nivel from nivel as n inner join (select sub.id_nivel,tp.clave_mun from subnivel as sub inner join tasa_poblacional as tp on(sub.id_subnivel=tp.id_subnivel)) as temp on(temp.id_nivel= n.id_nivel) where temp.clave_mun='$clave_mun' AND n.id_nivel='$id_nivel'");
  if (!$niveles) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($niveles as $niv) {
    $datos['niveles'][] = $niv;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('niveles.php', $datos);
});


$app->get('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles', function($clave_entidad,$clave_mun,$id_nivel) use($app) {
  $datos = array('sub_niveles' => array());
  $sub_niveles = $app->db->query("select distinct sub.id_nivel,sub.id_subnivel,sub.subnivel from subnivel as sub inner join tasa_poblacional as tp on(sub.id_subnivel=tp.id_subnivel) where tp.clave_mun='$clave_mun' AND sub.id_nivel='$id_nivel' order by sub.id_subnivel");
  if (!$sub_niveles) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($sub_niveles as $niv) {
    $datos['sub_niveles'][] = $niv;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('subniveles.php', $datos);
});

$app->get('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles/:id_subnivel', function($clave_entidad,$clave_mun,$id_nivel,$id_subnivel) use($app) {
  $datos = array('sub_niveles' => array());
  $sub_niveles = $app->db->query("select distinct sub.id_nivel,sub.id_subnivel,sub.subnivel from subnivel as sub inner join tasa_poblacional as tp on(sub.id_subnivel=tp.id_subnivel) where tp.clave_mun='$clave_mun' AND sub.id_nivel='$id_nivel' AND sub.id_subnivel='$id_subnivel' order by sub.id_subnivel");
  if (!$sub_niveles) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($sub_niveles as $niv) {
    $datos['sub_niveles'][] = $niv;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('subniveles.php', $datos);
});

$app->get('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles/:id_subnivel/indicadores', function($clave_entidad,$clave_mun,$id_nivel,$id_subnivel) use($app) {
  $datos = array('indicadores' => array());
  $indicadores = $app->db->query("select distinct id_indicador , indicador from indicador as ind inner join (select tp.id_identificador from subnivel as sub inner join tasa_poblacional as tp on(sub.id_subnivel=tp.id_subnivel) where tp.clave_mun='$clave_mun' AND sub.id_nivel='$id_nivel' AND sub.id_subnivel='$id_subnivel' ) as tempo on(tempo.id_identificador = ind.id_indicador) ");
  if (!$indicadores) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($indicadores as $ind) {
    $datos['indicadores'][] = $ind;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('indicadores.php', $datos);
});


$app->get('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles/:id_subnivel/indicadores/:id_ind', function($clave_entidad,$clave_mun,$id_nivel,$id_subnivel,$id_ind) use($app) {
  $datos = array('indicadores' => array());
  $indicadores = $app->db->query("select distinct id_indicador , indicador from indicador as ind inner join (select tp.id_identificador from subnivel as sub inner join tasa_poblacional as tp on(sub.id_subnivel=tp.id_subnivel) where tp.clave_mun='$clave_mun' AND sub.id_nivel='$id_nivel' AND sub.id_subnivel='$id_subnivel' ) as tempo on(tempo.id_identificador = ind.id_indicador) where id_indicador='$id_ind' ");
  if (!$indicadores) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($indicadores as $ind) {
    $datos['indicadores'][] = $ind;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('indicadores.php', $datos);
});

$app->get('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles/:id_subnivel/indicadores/:id_ind/anios', function($clave_entidad,$clave_mun,$id_nivel,$id_subnivel,$id_ind) use($app) {
  $datos = array('indicadores' => array());
  $anio = $app->db->query("select an.id_anio , anio from anio as an inner join (select tp.id_anio from subnivel as sub inner join tasa_poblacional as tp on(sub.id_subnivel=tp.id_subnivel) where tp.clave_mun='$clave_mun' AND sub.id_nivel='$id_nivel' AND sub.id_subnivel='$id_subnivel' AND id_identificador='$id_ind') as tempo on(tempo.id_anio = an.id_anio)");
  if (!$anio) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($anio as $an) {
    $datos['anio'][] = $an;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('anios.php', $datos);
});


$app->get('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles/:id_subnivel/indicadores/:id_ind/anios/:id_anio', function($clave_entidad,$clave_mun,$id_nivel,$id_subnivel,$id_ind,$id_anio) use($app) {
  $datos = array('indicadores' => array());
  $anio = $app->db->query("select t3.id_tasa,t3.municipio,an.anio,t3.indicador,t3.subnivel,t3.cantidad from (select t2.id_tasa,t2.id_anio,inn.indicador,t2.subnivel,t2.cantidad,t2.municipio from (select t1.id_tasa,t1.id_anio,t1.id_identificador,t1.subnivel,t1.cantidad,m.municipio from (select tp.id_tasa,tp.id_anio,tp.id_identificador,sub.subnivel,tp.cantidad,tp.clave_mun from subnivel as sub inner join tasa_poblacional as tp on(sub.id_subnivel=tp.id_subnivel) where tp.clave_mun='$clave_mun' AND sub.id_nivel='$id_nivel' AND sub.id_subnivel='$id_subnivel' AND id_identificador='$id_ind') as t1 inner join municipio as m on(t1.clave_mun=m.clave_mun))as t2 inner join indicador as inn on(t2.id_identificador=inn.id_indicador))as t3 inner join anio as an on (t3.id_anio=an.id_anio) where an.id_anio='$id_anio'");
  if (!$anio) {
    $app->halt(500, "Error: En estos momentos no se puede acceder a la informacion de las entidades.");
  }
  foreach ($anio as $an) {
    $datos['anio'][] = $an;
  }
  $app->response->headers->set('Content-Type', 'application/xml');
  $app->response->setStatus(200);
  $app->render('anio.php', $datos);
});

//DELETE
$app->delete('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles/:id_subnivel/indicadores/:id_ind/anios/:id_anio', function($clave_entidad,$clave_mun,$id_nivel,$id_subnivel,$id_ind,$id_anio) use($app) {

  $anio = $app->db->query("delete from tasa_poblacional where id_anio='$id_anio' AND id_identificador='$id_ind' AND id_subnivel='$id_subnivel' AND clave_mun='$clave_mun'");

  if (!$anio) {
    $app->halt(500, "Error: No se ha podido borrar la tasa poblacional  con el id año:'$id_anio' identificador:'$id_ind' subnivel='$id_subnivel' municipio:'$clave_mun' ");
  }

  if ($anio->rowCount() != 1) {
    $app->halt(404, "Error: La tasa poblacional  con el id año:'$id_anio' identificador:'$id_ind' subnivel='$id_subnivel' municipio:'$clave_mun'  no existe.");
  }
  $app->halt(200, "Exito: La tasa poblacional  con el id año:'$id_anio' identificador:'$id_ind' subnivel='$id_subnivel' municipio:'$clave_mun' ha sido borrado");
});

//entidades
$app->delete('/entidades/:clave_entidad', function($clave_entidad) use($app) {

  $entidades = $app->db->query("select * from entidad where clave_entidad='$clave_entidad'");

  if ($entidades->rowCount() >= 1) {
    $entidades = $app->db->query("select * from municipio as m where clave_entidad='$clave_entidad'");
    foreach ($entidades as $ent){
      $temp=$ent['clave_mun'];
      $delet = $app->db->query("delete from tasa_poblacional where clave_mun='$temp'");
    }
    $entidades = $app->db->query("delete from municipio where clave_entidad='$clave_entidad'");
    $entidades = $app->db->query("delete from entidad where clave_entidad='$clave_entidad'");
    $app->halt(200, "Exito: La entidad  con la  clave:'$clave_entidad' ha sido borrado");
  }
  else{
    $app->halt(404, "Error: La entidad  con la clave '$clave_entidad' no existe.");
  }
  
});

//municipios
$app->delete('/entidades/:clave_entidad/municipios/:clave_mun', function($clave_entidad,$clave_mun) use($app) {
  $municipios = $app->db->query("select * from municipio where clave_entidad='$clave_entidad' AND clave_mun='$clave_mun'");
  if ($municipios->rowCount() >= 1) {
    $municipios = $app->db->query("delete from tasa_poblacional where clave_mun='$clave_mun'");
    $municipios = $app->db->query("delete from municipio where clave_entidad='$clave_entidad' AND clave_mun='$clave_mun'");
    $app->halt(200, "Exito: El municipio  con la  clave:'$clave_mun' ha sido borrado");
  }
  else{
    $app->halt(404, "Error: El municipio  con la clave '$clave_mun' no existe.");
  }
});

//niveles
$app->delete('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel', function($clave_entidad,$clave_mun,$id_nivel) use($app) {
  $niveles = $app->db->query("select * from nivel where id_nivel='$id_nivel'");
  if ($niveles->rowCount() >= 1) {
    $niveles = $app->db->query("select * from tasa_poblacional as tp inner join subnivel as s on(s.id_subnivel=tp.id_subnivel) where id_nivel='$id_nivel' AND clave_mun='$clave_mun'");
    foreach ($niveles as $niv){
      $temp = $niv['id_tasa'];
      $delet = $app->db->query("delete from tasa_poblacional where id_tasa='$temp'");
    }
    $niveles = $app->db->query("delete from subnivel where id_nivel='$id_nivel'");
    $niveles = $app->db->query("delete from nivel where id_nivel='$id_nivel'");
    $app->halt(200, "Exito: El nivel  con el id '$id_nivel' ha sido borrado");
  }
  else{
    $app->halt(404, "Error: El nivel  con el id '$id_nivel' no existe.");
  }
});

//subniveles
$app->delete('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles/:id_subnivel', function($clave_entidad,$clave_mun,$id_nivel,$id_subnivel) use($app) {
  $sub_niveles = $app->db->query("select * from subnivel where id_subnivel='$id_subnivel'");
  if ($sub_niveles->rowCount() >= 1) {
    $niveles = $app->db->query("delete from tasa_poblacional where clave_mun='$clave_mun' AND id_subnivel='$id_subnivel'");
    $niveles = $app->db->query("delete from subnivel where id_nivel='$id_nivel' AND id_subnivel='$id_subnivel'");
    $app->halt(200, "Exito: El subnivel  con el id '$id_subnivel' ha sido borrado");
  }
  else{
    $app->halt(404, "Error: El subnivel  con el id '$id_subnivel' no existe.");
  }
});


//POST inserta
//anio
$app->post('/anios/:an', function($an) use($app) {

  $anio = $app->db->query("select * from anio where anio='$an'");
 
  if ($anio->rowCount() >= 1) {
    $app->halt(404, "Error: Anio dublicado.");
  }
  else{
    $id_ultimo = $app->db->query("select id_anio from anio order by id_anio desc limit 1");
    $temp;
    foreach ($id_ultimo as $id){
      $temp=$id['id_anio']+1;
    }
    $anio = $app->db->query("insert into anio values('$temp','$an')");
    $app->halt(200, "Exito: El año '$an' ha sigo agregado");
  }
});

//POST entidades
$app->post('/entidades/:entidad', function($entidad) use($app) {

  $entidades = $app->db->query("select * from entidad where entidad='$entidad'");
 
  if ($entidades->rowCount() >= 1) {
    $app->halt(404, "Error: Entidad dublicado.");
  }
  else{
    $id_ultimo = $app->db->query("select clave_entidad from entidad order by clave_entidad desc limit 1");
    $temp;
    foreach ($id_ultimo as $id){
      $temp=$id['clave_entidad']+1;
    }
    $entidades = $app->db->query("insert into entidad values('$temp','$entidad')");
    $app->halt(200, "Exito: La entidad '$entidad' ha sigo agregado");
  }
});

//POST municipio

$app->post('/entidades/:clave_entidad/municipios/:municipio', function($clave_entidad,$municipio) use($app) {

  $municipios = $app->db->query("select * from entidad where clave_entidad='$clave_entidad'");
  $xx = $app->db->query("select * from municipio where municipio='$municipio' AND clave_entidad='$clave_entidad'");
  if ($xx->rowCount() >= 1 && $municipios->rowCount()>= 1) {
    $app->halt(404, "Error: Municipio dublicado.");
  }
  else{
    $id_ultimo = $app->db->query("select clave_mun from municipio order by clave_mun desc limit 1");
    $temp;
    foreach ($id_ultimo as $id){
      $temp=$id['clave_mun']+1;
    }
    $municipios = $app->db->query("insert into municipio values('$clave_entidad','$temp','$municipio')");
    $app->halt(200, "Exito: El municipio '$municipio' ha sigo agregado");
  }
});

//PUT actualiza cantidad
$app->put('/entidades/:clave_entidad/municipios/:clave_mun/niveles/:id_nivel/subniveles/:id_subnivel/indicadores/:id_ind/anios/:id_anio/:cantidad', function($clave_entidad,$clave_mun,$id_nivel,$id_subnivel,$id_ind,$id_anio,$cantidad) use($app)  {

  $cantidades = $app->db->query("update tasa_poblacional set cantidad='$cantidad' where id_anio='$id_anio' AND id_identificador='$id_ind' AND id_subnivel='$id_subnivel' AND clave_mun='$clave_mun'");
  
  if (!$cantidades) {
    $app->halt(500, "Error: No se ha podido editar la tasa poblacional  con el id año:'$id_anio' identificador:'$id_ind' subnivel='$id_subnivel' municipio:'$clave_mun' ");
  }

  if ($cantidades->rowCount() != 1) {
    $app->halt(404, "Error: La tasa poblacional  con el id año:'$id_anio' identificador:'$id_ind' subnivel='$id_subnivel' municipio:'$clave_mun'  no existe.");
  }
  $app->halt(200, "Exito: La tasa poblacional  con el id año:'$id_anio' identificador:'$id_ind' subnivel='$id_subnivel' municipio:'$clave_mun' ha sido actualizado");
});

//actualizar entidad
$app->put('/entidades/:clave_entidad/:entidad', function($clave_entidad,$entidad) use($app)  {

  $entidades = $app->db->query("update entidad set entidad='$entidad' where clave_entidad='$clave_entidad'");
  
  if (!$entidades) {
    $app->halt(500, "Error: No se ha podido editar la entidad con la clave '$clave_entidad'");
  }

  if ($entidades->rowCount() != 1) {
    $app->halt(404, "Error: La entidad con la clave '$clave_entidad' no existe.");
  }
  $app->halt(200, "Exito: La entidad con la clave '$clave_entidad' ha sido actualizado");
});

//actualizar municipios
$app->put('/entidades/:clave_entidad/municipios/:clave_mun/:municipio', function($clave_entidad,$clave_mun,$municipio) use($app)  {

  $municipios = $app->db->query("update municipio set municipio='$municipio' where clave_entidad='$clave_entidad' AND clave_mun='$clave_mun'");
  
  if (!$municipios) {
    $app->halt(500, "Error: No se ha podido editar el municipio con la clave '$clave_mun'");
  }

  if ($municipios->rowCount() != 1) {
    $app->halt(404, "Error: El municipio con la clave '$clave_mun' no existe.");
  }
  $app->halt(200, "Exito: El municipio con la clave '$clave_mun' ha sido actualizado");
});
