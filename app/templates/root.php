<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Poblacion, Hogares y Viviendas</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" href="../css/main.css">
<script src="../js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>
<body onload="Aplicacion.main();">
  <!--[if lt IE 7]>
  <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->
  <div class="navbar navbar-default" role="navigation">
    <div class="container-fluid">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">Poblacion, Hogares y Viviendas</a>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav"><li class="active"><a href="/">Inicio</a></li></ul>
	</div>
    </div>
  </div>
	 </div>
 
<div class="container-fluid">
<div class="row-fluid">
<div class="span10">
     
	 </div>
<div align="center"><img src="http://www.bicimapas.com.mx/images/Logo%20Inegi.gif"></div>
	<div class="navbar-collapse collapse">
	<ul class="list-unstyled"><strong><li class="info">RECURSOS DISPONIBLES</li></strong></ul>

        <ul class="list-unstyled"><li class="info"><a href="/api/entidades">Entidades</a></li></ul>
	<ul>
        	<ul class="list-unstyled"><li class="info"><a href="/api/entidades/00/municipios">Municipios</a></li></ul>
			<ul>
				<ul class="list-unstyled"><li class="info"><a href="/api/entidades/00/municipios/00/niveles">Niveles</a></li></ul>
					<ul>
						<ul class="list-unstyled"><li class="info"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles">Subnivel-Natalidad y Fecundidad</a></li></ul>
						<ul class="list-unstyled"><li class="info"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles">Subnivel-Mortalidad</a></li></ul>
						<ul class="list-unstyled"><li class="info"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles">Subnivel-Nupcialidad</a></li></ul>
					</ul>
			</ul>
	</ul>
</div>
<div class="navbar-collapse collapse">    
	<ul class="list-unstyled"><strong><li class="info">TIPO DE INDICADORES</li></strong></ul>  
	<ul class="list-unstyled"><li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores">Natalidad</a></li></ul>
	<ul class="list-unstyled"><li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores">Mortalidad</a></li></ul>
	<ul class="list-unstyled"><li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores">Matrimonios</a></li></ul>
	<ul class="list-unstyled"><li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores">Divorcios</a></li></ul>
</div>
<div name="Tasa poblacional "class="navbar-collapse collapse">  
	<ul class="list-unstyled"><strong><li class="active">TASA POBLACIONAL</li></strong></ul>
	<ul class="nav navbar-nav">
		<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios">Nacimientos</a></li>
		<ul class="nav navbar-nav"> 
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/01"> 1994 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/02"> 1995 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/03"> 1996 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/04"> 1997 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/05"> 1998 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/06"> 1999 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/07"> 2000 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/08"> 2001 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/09"> 2002 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/10"> 2003 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/11"> 2004 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/12"> 2005 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/13"> 2006 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/14"> 2007 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/15"> 2008 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/16"> 2009 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/17"> 2010 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/18"> 2011 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/01/subniveles/101/indicadores/1002000026/anios/19"> 2012 </a></li>
		
		</ul>
		<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios"> Mortalidad </a></li>
		<ul class="nav navbar-nav">
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/01"> 1994 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/02"> 1995 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/03"> 1996 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/04"> 1997 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/05"> 1998 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/06"> 1999 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/07"> 2000 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/08"> 2001 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/09"> 2002 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/10"> 2003 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/11"> 2004 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/12"> 2005 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/13"> 2006 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/14"> 2007 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/15"> 2008 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/16"> 2009 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/17"> 2010 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/18"> 2011 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/02/subniveles/201/indicadores/1002000032/anios/19"> 2012 </a></li>
		
		</ul>
		<li class="info"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios"> Matrimonios </a></li>
		<ul class="nav navbar-nav"> 
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/01"> 1994 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/02"> 1995 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/03"> 1996 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/04"> 1997 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/05"> 1998 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/06"> 1999 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/07"> 2000 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/08"> 2001 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/09"> 2002 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/10"> 2003 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/11"> 2004 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/12"> 2005 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/13"> 2006 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/14"> 2007 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/15"> 2008 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/16"> 2009 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/17"> 2010 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/18"> 2011 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/301/indicadores/1002000038/anios/19"> 2012 </a></li>
		
		</ul>
		<li class="info"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios"> Divorcios </a></li>
		<ul class="nav navbar-nav">
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/01"> 1994 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/02"> 1995 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/03"> 1996 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/04"> 1997 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/05"> 1998 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/06"> 1999 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/07"> 2000 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/08"> 2001 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/09"> 2002 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/10"> 2003 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/11"> 2004 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/12"> 2005 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/13"> 2006 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/14"> 2007 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/15"> 2008 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/16"> 2009 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/17"> 2010 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/18"> 2011 </a></li>
			<li class="active"><a href="/api/entidades/00/municipios/00/niveles/03/subniveles/302/indicadores/1002000039/anios/19"> 2012 </a></li>
		
		</ul>
	</ul>
</div>
</div>
</div>
   
  <script src="js/vendor/jquery-1.11.0.min.js"></script>
  <script src="js/vendor/bootstrap.min.js"></script>
  <!--<script src="js/prac07.js"></script>-->
  </body>
</html>
