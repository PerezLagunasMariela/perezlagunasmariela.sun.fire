<?xml version="1.0" encoding="UTF-8"?>
<catalog>
<?php foreach ($anio as $an) { ?>
  <anios id="<?php echo $an['id_tasa']; ?>">
    <municipio> <?php echo $an['municipio']; ?> </municipio>
    <anio><?php echo $an['anio']; ?> </anio>
    <indicador> <?php echo $an['indicador']; ?> </indicador>
    <subnivel> <?php echo $an['subnivel']; ?> </subnivel>
    <cantidad> <?php echo $an['cantidad']; ?> </cantidad>
  </anios>
<?php } ?>
</catalog>
