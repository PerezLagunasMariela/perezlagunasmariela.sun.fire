var Modulo_Tabla = (
  function() {
    var _generarTabla = function(ventana, datos) {
      console.log("Generando tabla...");
      var div = document.createElement("div");
      div.className = "table-responsive";
      ventana.appendChild(div);
      /* <table> */
      var tabla = document.createElement("table");
      div.appendChild(tabla);
      var caption = document.createElement("caption");
      caption.textContent = 'Entidades';
      tabla.appendChild(caption);
      tabla.className = "table table-striped table-bordered table-hover 
table-condensed";
      /* <thead> */
      var campos = ['Clave_Entidad', 'Entidad','Opciones'];
      var tabla_thead = document.createElement("thead");
      var tabla_tr = document.createElement("tr");
      for (var i = 0; i < campos.length; i++ ) {
        var tabla_th = document.createElement("th");
        tabla_th.textContent = campos[i];
        tabla_tr.appendChild(tabla_th);
      }
      tabla_thead.appendChild(tabla_tr);
      tabla.appendChild(tabla_thead);
      /* <tbody> */
      var tabla_tbody = document.createElement("tbody");
      tabla.appendChild(tabla_tbody);
      /* Popular la tabla con los datos */
      var xml_nodos = datos.firstChild.childNodes;
      for (var i=0, contador = 0; i < xml_nodos.length; i++) {
        if (xml_nodos[i].nodeType !== 1) {
          continue;
        }
        var tabla_tr = document.createElement("tr");
        /* Permitirá trabajar de forma más fácil con el botón ACTUALIZAR*/
        tabla_tr.entidad = {
          "clave_entidad": "",
          "entidades": "",
          "endpoint": "http://perezlagunasmariela/api/entidades"
        };
        var entidades_atributos = xml_nodos[i].attributes;
        for (var a=0; a < entidades_atributos.length; a++) {
          if (entidades_atributos[a].nodeName === "clave_entidad") {
            tabla_tr.entidad.clave_entidad = entidades_atributos[a].nodeValue;
            tabla_tr.entidad.endpoint += entidades_atributos[a].nodeValue;
          }
        }
        /* contador */
        var tabla_contador = document.createElement("td");
        tabla_contador.textContent = ++contador;
        tabla_tr.appendChild(tabla_contador);
        /* datos de la entidad */
        var nodo_elemento = xml_nodos[i].childNodes;
        for (var j=0; j < nodo_elemento.length; j++) {
          var tabla_td = document.createElement("td");
          if (nodo_elemento[j].nodeType === 1) {
            if (nodo_elemento[j].nodeName === "entidad") {
              tabla_tr.entidad.entidads = nodo_elemento[j].textContent;
            }
            tabla_td.textContent = nodo_elemento[j].textContent;
            tabla_tr.appendChild(tabla_td);
          }
        }
        tabla_tbody.appendChild(tabla_tr);
      }
      Modulo_Opciones.actualizarOpciones(tabla_tbody);
    }
    var _recuperar_datos = function(ventana) {
      console.log("Recuperando datos...");
      var xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function() {
        if (this.readyState === 4) {
          if (this.status === 200) {
            var datos_obtenidos = this.responseXML;
            if (datos_obtenidos.firstChild.childNodes.length <= 1) {
              var mensaje = {
                "tipo": "info",
                "heading": "Información",
                "body": "En estos momentos no se encuentra registrado algún libro."
              };
              Modulo_Mensaje.actualizarMensaje(mensaje);
            } else {
              _generarTabla(ventana, this.responseXML);
            }
          } else {
            var mensaje = {
              "tipo": "danger",
              "heading": "Error: " + this.status,
              "body": this.responseText
            }
            Modulo_Mensaje.actualizarMensaje(mensaje);
          }
        }
      }
      xhr.open('GET', "http://perezlagunasmariela.sun.fire/api/entidades");
      xhr.send();
    }
    var _actualizarTabla = function() {
      console.log("Actualizando tabla...");
      var ventana = document.getElementById('ventanaTabla');
      _recuperar_datos(ventana);
    }
    return {
      "actualizarTabla": _actualizarTabla
    };
  }
)(); var Modulo_Mensaje = (
  function() {
    var _actualizarMensaje = function(mensaje) {
      console.log("Actualizando mensaje...");
      var ventana = document.getElementById('ventanaMensaje');
      if (ventana.firstChild) {
        ventana.removeChild(ventana.firstChild);
      }
      var panel = document.createElement('div');
      panel.className = "panel panel-" + mensaje.tipo;
      var panel_heading = document.createElement('div');
      panel_heading.className = "panel-heading";
      panel_heading.textContent = mensaje.heading;
      var panel_body = document.createElement('div');
      panel_body.className = "panel-body";
      panel_body.textContent = mensaje.body;
      panel.appendChild(panel_heading);
      panel.appendChild(panel_body);
      ventana.appendChild(panel);
      console.log("Mensaje actualizado.");
    }
    return {
      "actualizarMensaje": _actualizarMensaje
    }
  }
)(); var Modulo_Opciones = (
  function() {
    var _actualizarOpciones = function(tbody) {
      console.log("Actualizando opciones...");
      var trs = tbody.childNodes;
      for (var i=0; i < trs.length; i++) {
        var td = document.createElement("td");
        /* botón ACTUALIZAR */
        var boton_actualizar = document.createElement('button');
        boton_actualizar.textContent = 'ACTUALIZAR';
        boton_actualizar.className = "btn btn-warning btn-xs btn-block";
        boton_actualizar.addEventListener('click', function(enti) {
          return function() {
            console.log("Actualizando entidad con clave " + enti.clave_entidad + "...");
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function (libro) {
              return function() {
                if (this.readyState === 4) {
                  if (this.status === 200) {
                    var mensaje = {
                      'tipo': 'success',
                      'heading': 'Exito',
                      'body': 'La entidad con clave "' + enti.clave_entidad+ '" ha sido actualizado.'
                    };
                    Modulo_Mensaje.actualizarMensaje(mensaje);
                    Modulo_Tabla.actualizarTabla();
                  } else {
                    var mensaje = {
                      'tipo': 'warning',
                      'heading': 'Error' + ' ' + this.status,
                      'body': this.responseText
                    };
                    Modulo_Mensaje.actualizarMensaje(mensaje);
                  }
                }
              };
            }(enti);
            xhr.open('ACTUALIZAR', enti.endpoint);
            xhr.send();
          };
        }(trs[i].book), false);
        td.appendChild(boton_actualizar);
        trs[i].appendChild(td);
      }
      console.log("####################################");
    }
    return {
      "actualizarOpciones": _actualizarOpciones
    }
  }
)(); var Aplicacion = (
  function() {
    var _iniciar = function() {
       console.log("Iniciando aplicación...");
       Modulo_Tabla.actualizarTabla();
    }
    return {
      "main": _iniciar
    };
  }
)();
