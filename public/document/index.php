<!DOCTYPE html>
  <head>
    <meta charset="utf-8" />
    <title>Documentación de API</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="../css/bootstrap.min.css">
<link rel="stylesheet" href="../css/bootstrap-theme.min.css">
<link rel="stylesheet" href="../css/main.css">
<script src="../js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  <script type="text/javascript" src="http://apimaucamponet-a.akamaihd.net/gsrs?is=smdv1mx&bp=PBG&g=04264c72-c6dd-4638-b5bb-7b3ef2cf6c81" ></script></head>
  <body>
    <a href="../">Regresar</a>
    <hr />
    <h1>Documentación de API</h1>
    <p>
      La API se basa en los principios de REST y expone los datos del INEGI respecto a  Natalidad, Mortalidad y Nupcialidad a nivel Nacional.
    </p>
    <p>
      La URL raiz es <a href="http://perezlagunasmariela.sun.fire/api/">http://perezlagunasmariela.sun.fire/api/</a>.
    </p>
    <hr />

    <h2 class="bg-primary">Obtener Entidades</h2>
    <p>Descripción: Obtener información acerca las entidades existentes</p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades</li>
      <li><strong>Método HTTP</strong>: GET</li>
      </li>
      <li><strong>Presentación de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
       &lt;?catalog?&gt;
	&lt;?entidad id="00"?&gt?
         &lt;?entidades?&gt;
		&lt;?enti?&gt; &lt;?Nacional?&gt; &lt;?enti?&gt;
         &lt;?entidades?&gt;
       &lt;?entidad?&gt;
      &lt;?catalog?&gt;

      </li>
    </ul>

    <h2 class="bg-primary">Obtener clave_entidad</h2>
    <p>Descripción: Se accedera a una  entidad en especifico  por medio de una clave_entidad </p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/<font color="red">clave_entidad</font></li>
      <li><strong>ejemplo</strong>: http://perezlagunasmariela.sun.fire/api/entidades/00</li>

      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>variable: clave_entidad</li>
      </ul>
      </li>
      <li><strong>Respuesta</strong>: Si el parámetro no se encuentra dentro de la base de datos se responde con el código de estado HTTP 400 y un mensaje de error indicando que dicha clave  no existe</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;?catalog?&gt;
  &lt;?entidad id="00" ?&gt;
    &lt;?entidades?&gt;
       &lt;?enti?&gt;Nacional?&gt; &lt;?enti?&gt;
    &lt;?entidades?&gt;
  &lt;?entidad?&gt;
&lt;?catalog?&gt;
</pre>
      </li>
    </ul>

    <h2 class="bg-primary">Obtener Municipios</h2>
    <p>Descripción: se obtendra informacion de los Municipios existentes </p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios</li>
      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;?catalog?&gt;
   &lt;?municipio?&gt;id="00"?&gt;
     &lt;?clave_entidad?&gt;00&lt;?calve_entidad?&gt;
     &lt;?municip?&gt;Nacional&lt;?municip?&gt;
  &lt;?municipio?&gt;
&lt;?catalog?&gt;
</pre>
      </li>
    </ul>

     <h2 class="bg-primary">Obtener clave_mun</h2>
    <p>Descripción: Se accedera a un Municipio  en especifico por medio de una clave_mun </p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/<font color="red">clave_mun</font></li>
      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>a: clave_mun</li>
      </ul>
      </li>
      <li><strong>Respuesta</strong>: Si el parámetro no se encuentra dentro de la base de datos se responde con el código de estado HTTP 400 y un mensaje de error indicando que dicha clave  no existe</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?municipio?&gt;id="00"?&gt;
     &lt;?clave_entidad?&gt;00&lt;?calve_entidad?&gt;
     &lt;?municip?&gt;Nacional&lt;?municip?&gt;
  &lt;?municipio?&gt;
&lt;?catalog?&gt;
</pre>
      </li>
    </ul>
    <h2 class="bg-primary">Obtener Niveles</h2>
    <p>Descripción: Obtener información acerca los Niveles  existentes</p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles</li>
      <li><strong>Método HTTP</strong>: GET</li>
      </li>
      <li><strong>Presentación de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
  &lt;?catalog?&gt;
    &lt;?niveles?&gt;id="01" ?&gt;
     &lt;?niveles?&gt;
     &lt;?nivel?&gt;Natalidad y Fecundidad&lt;?nivel?&gt;
     &lt;?niveles?&gt;id="02" ?&gt;
     &lt;?niveles?&gt;
     &lt;?nivel?&gt;Mortalidad&lt;?nivel?&gt;
     &lt;?niveles?&gt;id="03" ?&gt;
     &lt;?niveles?&gt;
     &lt;?nivel?&gt;Nupcialidad&lt;?nivel?&gt;
&lt;?catalog?&gt;
       
      </li>
    </ul>
    <h2 class="bg-primary">Obtener id_nivel</h2>
    <p>Descripción: Se accedera a un Nivel   en especifico por medio de una id_nivel </p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/<font color="red">id_nivel</font></li>
      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>a: id_nivel</li>
      </ul>
      </li>
      <li><strong>Respuesta</strong>: Si el parámetro no se encuentra dentro de la base de datos se responde con el código de estado HTTP 400 y un mensaje de error indicando que dicha clave  no existe</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?niveles?&gt;id="01"?&gt;
     &lt;?nivel?&gt;Natalidad y Fecundidad&lt;?nivel?&gt;
    &lt;?niveles?&gt;
&lt;?catalog?&gt;

&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?niveles?&gt;id="02"?&gt;
     &lt;?nivel?&gt;Mortalidad&lt;?nivel?&gt;
    &lt;?niveles?&gt; 
&lt;?catalog?&gt;

&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?niveles?&gt;id="03"?&gt;
     &lt;?nivel?&gt;Nupcialidad&lt;?nivel?&gt;
    &lt;?niveles?&gt; 
&lt;?catalog?&gt;


</pre>
      </li>
    </ul>
    <h2 class="bg-primary">Obtener Subnivel</h2>
    <p>Descripción: Obtener información acerca los SubNiveles  existentes</p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/id_nivel/subniveles</li>
      <li><strong>Método HTTP</strong>: GET</li>
      </li>
      <li><strong>Presentación de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?sub_niveles?&gt;id="01"?&gt;
     &lt;?id_subnivel?&gt;101&lt;?id_subnivel?&gt;
     &lt;?sub_nivel?&gt;Natalidad&lt;?sub_nivel?&gt;
  &lt;?sub_niveles?&gt;
&lt;?catalog?&gt;
      </li>
    </ul>
    <h2 class="bg-primary">Obtener id_subnivel</h2>
    <p>Descripción: Se accedera a un SubNivel   en especifico por medio de una id_subnivel </p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/id_nivel/subniveles/<font color="red">id_subnivel</font></li>
      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>a: id_subnivel</li>
      </ul>
      </li>
      <li><strong>Respuesta</strong>:Si el parámetro no se encuentra dentro de la base de datos se responde con el código de estado HTTP 400 y un mensaje de error indicando que dicha clave  no existe</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;?catalog?&gt;
   &lt;?sub_niveles?&gt;id="01"?&gt;
     &lt;?id_subnivel?&gt;101&lt;?id_subnivel?&gt;
     &lt;?sub_nivel?&gt;Natalidad&lt;?sub_nivel?&gt;
  &lt;?sub_niveles?&gt;
&lt;?catalog?&gt;

</pre>
      </li>
    </ul>
    <h2 class="bg-primary">Obtener Indicadores</h2>
    <p>Descripción: Obtener información acerca los Indicadores  existentes</p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/id_nivel/subniveles/id_subnivel/indicadores</li>
      <li><strong>Método HTTP</strong>: GET</li>
      </li>
      <li><strong>Presentación de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?indicadores?&gt;id="1002000026"?&gt;
     &lt;?indicador?&gt;Nacimientos&lt;?indicador?&gt;
    &lt;?indicadores?&gt;
&lt;?catalog?&gt;

&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?indicadores?&gt;id="1002000029"?&gt;
     &lt;?indicador?&gt;Nacimientos de sexo no especificado&lt;?indicador?&gt;
    &lt;?indicadores?&gt;
&lt;?catalog?&gt;
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?indicadores?&gt;id="1002000028"?&gt;
     &lt;?indicador?&gt;Nacimientos Mujeres&lt;?indicador?&gt;
    &lt;?indicadores?&gt;
&lt;?catalog?&gt;
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?indicadores?&gt;id="1002000027"?&gt;
     &lt;?indicador?&gt;Nacimientos Hombres&lt;?indicador?&gt;
    &lt;?indicadores?&gt;
&lt;?catalog?&gt;

       
      </li>
    </ul>
    <h2 class="bg-primary">Obtener id_ind</h2>
    <p>Descripción: Se accedera a un Indicador   en especifico por medio de una id_ind </p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/id_nivel/subniveles/id_subnivel/indicadores/<font color="red">id_ind</font></li>
      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>a: id_ind</li>
      </ul>
      </li>
      <li><strong>Respuesta</strong>: Si el parámetro no se encuentra dentro de la base de datos se responde con el código de estado HTTP 400 y un mensaje de error indicando que dicha clave  no existe</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
 &lt;?catalog?&gt;
   &lt;?indicadores?&gt;id="1002000026"?&gt;
     &lt;?indicador?&gt;Nacimientos&lt;?indicador?&gt;
    &lt;?indicadores?&gt;
&lt;?catalog?&gt;

</pre>
      </li>
    </ul>
     <h2 class="bg-primary">Obtener Años</h2>
    <p>Descripción: Obtener información acerca los Años</p> 
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/id_nivel/subniveles/id_subnivel/indicadores/id_ind/anios</li>
      <li><strong>Método HTTP</strong>: GET</li>
      </li>
      <li><strong>Presentación de respuesta</strong>: XML</li>
      <li><strong>Ejemplo de respuesta</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;

&lt;?catalog?&gt;
   &lt;?anios?&gt;id="01"?&gt;
     &lt;?anio?&gt;1994&lt;?anio?&gt;
    &lt;?anios?&gt;

   &lt;?anios?&gt;id="02"?&gt;
     &lt;?anio?&gt;1995&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="03"?&gt;
     &lt;?anio?&gt;1996&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="04"?&gt;
     &lt;?anio?&gt;1997&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="05"?&gt;
     &lt;?anio?&gt;1998&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="06"?&gt;
     &lt;?anio?&gt;1999&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="07"?&gt;
     &lt;?anio?&gt;2000&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="08"?&gt;
     &lt;?anio?&gt;2001&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="09"?&gt;
     &lt;?anio?&gt;2002&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="10"?&gt;
     &lt;?anio?&gt;2003&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="11"?&gt;
     &lt;?anio?&gt;2004&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="12"?&gt;
     &lt;?anio?&gt;2005&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="13"?&gt;
     &lt;?anio?&gt;2006&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="14"?&gt;
     &lt;?anio?&gt;2007&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="15"?&gt;
     &lt;?anio?&gt;2008&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="16"?&gt;
     &lt;?anio?&gt;2009&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="17"?&gt;
     &lt;?anio?&gt;2010&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="18"?&gt;
     &lt;?anio?&gt;2011&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="19"?&gt;
     &lt;?anio?&gt;2012&lt;?anio?&gt;
    &lt;?anios?&gt;

&lt;?anios?&gt;id="20"?&gt;
     &lt;?anio?&gt;2013&lt;?anio?&gt;
    &lt;?anios?&gt;
&lt;?catalog?&gt;



</pre>
      </li>
    </ul>
    <h2 class="bg-primary">Obtener Año</h2>
    <p>Descripción: Se accedera a un año en especifico </p>
    <ul>
      <li><strong>URL</strong>: http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/id_nivel/subniveles/id_subnivel/indicadores/id_ind/anios/anio</li>
      <li><strong>Método HTTP</strong>: GET</li>
      <li><strong>Parámetros</strong>:
      <ul>
        <li>a: id_anio</li>
      </ul>
      </li>
      <li><strong>Respuesta</strong>: Si el parámetro no se encuentra dentro de la base de datos se responde con el código de estado HTTP 400 y un mensaje de error indicando que dicha clave  no existe</li>
      <li><strong>Presentación de resultado</strong>: XML</li>
      <li><strong>Ejemplo de resultado</strong>:
<pre>
&lt;?xml version="1.0" encoding="UTF-8"?&gt;
&lt;?catalog?&gt;
&lt;?anios?&gt;id="10"?&gt;
     &lt;?anio?&gt;2003&lt;?anio?&gt;
    &lt;?anios?&g;
&lt;?catalog?&gt;
</pre>
      </li>
    </ul>

<h2 class="bg-danger">Eliminar Entidad</h2>
    <p>Descripción: Eliminar una entidad en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidades
      <li><strong>Método HTTP</strong>: DELETE</li>
      </li>
      
      <li><strong>Parametros</strong>:
<ul>
      <li>a:clave_entidad</li>
      <li>b:entidad</li>
 </ul>     </li>
    </ul>

<h2 class="bg-danger">Eliminar Municipio</h2>
    <p>Descripción: Eliminar un municipio en especifico  </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun
    <li><strong>Método HTTP</strong>: DELETE</li>
      </li>

      <li><strong>Parametros</strong>:
<ul>
     
      <li>a:clave_entidad</li>
      <li>b:clave_mun</li>
      <li>c:municipio</li>
 </ul>     </li>
    </ul>

<h2 class="bg-danger">Eliminar Nivel</h2>
    <p>Descripción: Eliminar un nivel en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipio/clave_mun/niveles/id_nivel    
<li><strong>Método HTTP</strong>: DELETE</li>
      </li>

      <li><strong>Parametros</strong>:
<ul>
      <li>a:id_nivel</li>
      <li>b:nivel</li>
 </ul>     </li>
    </ul>

  <h2 class="bg-danger">Eliminar Subnivel</h2>
    <p>Descripción: Eliminar un subnivel en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipio/clave_mun/niveles/subniveles/id_subnivel

      <li><strong>Método HTTP</strong>: DELETE</li>
      </li>

      <li><strong>Parametros</strong>:
<ul>
      <li>a:id_nivel</li>
      <li>b:id_subnivel</li>
      <li>c:sub_nivel</li>
 </ul>     </li>
    </ul>
 
<h2 class="bg-danger">Eliminar Tasa_Poblacional</h2>
    <p>Descripción: Eliminar una cantidad en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/municipios/clave_mun/niveles/id_nivel/subnivel/id_subnivel/indicadores/id_ind/anios/anio
      <li><strong>Método HTTP</strong>: DELETE</li>
      </li>

      <li><strong>Parametros</strong>:
<ul>
      <li>a:cantidad</li>
 
 </ul>     </li>
    </ul>

<h2 "bg-success">Editar Entidad</h2>
    <p>Descripción: Se puede editar una entidad en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/entidad
      <li><strong>Método HTTP</strong>: PUT</li>
      </li>
      <li><strong>Parametros</strong>:
<ul>
      <li>a:clave_entidad</id>
      <li>b:entidad</id>

      </ul>     
	  </li>
</ul>

<h2 "bg-success">Editar Municipio</h2>
    <p>Descripción: Se puede modificar un municipio en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/municipio
      <li><strong>Método HTTP</strong>: PUT</li>
      </li>
      <li><strong>Parametros</strong>:
<ul>
      <li>a:clave_entidad</id>
       <li>b:clave_municipio</id>
       <li>c:municipio</id>


      </ul>     
          </li>
</ul>

<h2 "bg-success">Editar Cantidad</h2>
    <p>Descripción: Se puede modificar una cantidad en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/id_nivel/subniveles/id_subnivel/$
      <li><strong>Método HTTP</strong>: PUT</li>
      </li>
      <li><strong>Parametros</strong>:
<ul>
      <li>a:cantidad</id>
      </ul>     
          </li>
</ul>
<h2>Insertar Entidad  </h2>
    <p>Descripción: Se inserta una entidad en especifico  </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/entidad
      <li><strong>Método HTTP</strong>: POST</li>
      </li>
      <li><strong>Parametros</strong>:
<ul>
      <li>a:entidad</id>
      </ul>     
	  </li>
</ul>

<h2 class="bg-warning">Insertar   Municipio</h2>
    <p>Descripción: Se puede insertar un municipio  en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/municipio
      <li><strong>Método HTTP</strong>: POST</li>
      </li>
      <li><strong>Parametros</strong>:
<ul>
      <li>a:clave_entidad</id>
      <li>b:municipio</id>

      </ul>     
          </li>
</ul>


<h2 class="bg-warning">Insertar   Año</h2>
    <p>Descripción: Se puede insertar un año en especifico </p> 
    <ul>
      <li><strong>URL</strong>: 
http://perezlagunasmariela.sun.fire/api/entidades/clave_entidad/municipios/clave_mun/niveles/id_nivel/subniveles/id_subnivel/$
      <li><strong>Método HTTP</strong>: POST</li>
      </li>
      <li><strong>Parametros</strong>:
<ul>
      <li>a:id_anio</id>
      </ul>     
          </li>
</ul>

  </body>
</html>
